import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buy-ins',
  templateUrl: './buy-ins.component.html',
  styleUrls: ['./buy-ins.component.scss']
})

export class BuyInsComponent implements OnInit {

  buyinType: string | undefined;
  togglePlayerSearch:boolean=false;
  toggleBackdrop:boolean=false;
  numkeys:any=[1,2,3,4,5,6,7,8,9,0]
  toggleNumpad:boolean=false;
  $buyinIndx = 0;
  tableTypeRoulette:boolean=true;
  // buyinTypes:any=['Cash','CPV','Front Money','Marker','eWallet','Debit Card'];
  buyinTypes:any=['Cash','CPV'];
  rouletteSeatsCountArray=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

  constructor() { }
  
  ngOnInit(): void {
    this.setVisibility();
  } 

  toggleKbd(e:any = ''){
    this.toggleNumpad = !this.toggleNumpad;
    console.log('e ',e.target.id)
  }

  setVisibility(){
    this.buyinType = 'CASH';
    this.tableTypeRoulette = true;
  }

  toggleBuyInTypes(e:any, index:any){
    // const $buyinType = e.target.innerHTML.toLowerCase();
    return this.$buyinIndx = index;
    
  }
}
