import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogComponent } from '../wd-dialog/wd-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-subnav-data-bar',
  templateUrl: './subnav-data-bar.component.html',
  styleUrls: ['./subnav-data-bar.component.scss', 'wd-alerts.component.scss']
})
export class SubnavDataBarComponent implements OnInit {
  animal: string = "";
  name: string = "";

  dataColumns = [
    {
      "label": "Buy In",
      "value": 595000000000,
      "count": 535,
      "rolled": null,
      "pending": null,
      "rolledIcon": null,
      "pendingIcon": null
    },
    {
      "label": "Cash Out",
      "value": 400000000000,
      "count": 250,
      "rolled": null,
      "pending": null,
      "rolledIcon": null,
      "pendingIcon": null
    },
    {
      "label": "Change",
      "value": 350000000000,
      "count": 425,
      "rolled": null,
      "pending": null,
      "rolledIcon": null,
      "pendingIcon": null
    },
    {
      "label": "Rolled",
      "value": null,
      "count": null,
      "rolled": 45,
      "pending": 16,
      "rolledIcon": null,
      "pendingIcon": null
    }
  ]

  
  
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openDialog(type:string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '30vw',
      data: {name: type, animal: this.animal},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  




}
