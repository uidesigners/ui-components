import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})



export class WdDesignServiceService {

  constructor() { }

  cagesLvl0 = new BehaviorSubject([
    {'name': 'First Card'},
    {'name': 'Treasury Macau'},
    {'name': 'Treasury Cotai'}
  ]);

  cages = new BehaviorSubject([
    {'name': 'Cage C01'},
    {'name': 'Cage C02'},
    {'name': 'Cage C03'},
    {'name': 'Cage C04'},
    {'name': 'Cage C05'},
    {'name': 'Cage C06'},
    {'name': 'Cage C07'},
    {'name': 'Cage C08'},
    {'name': 'Cage C09'},
    {'name': 'Cage C10'},
    {'name': 'Cage C11'}
  ]);

 
}
