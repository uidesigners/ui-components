import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-nav-bar',
  templateUrl: './app-top-nav-bar.component.html',
  styleUrls: ['./app-top-nav-bar.component.scss']
})
export class AppTopNavBarComponent implements OnInit {

  @Input() showGamingDay: boolean = true;
  @Input() nextRollTIme: any;
  @Input() gamingDay: string = '';
  @Input() gamingDaysArr: Array<any> = ['09-Feb', '10-Feb'];
  @Input() appName: string = '';

  loginPagesFlag: boolean = false;
  calendarDate: any;
  isAppOpened: boolean = true;
  employeeNumber: number = 199001;
  userDetails: any = ['Chinese', 'Logout'];
  lastRefresh: boolean = true;
  appTitle: string = "Unknown";


  constructor() { }

  ngOnInit(): void {
    if(this.appName == 'cashWin') {
      this.appTitle = 'Cashier Window';
    } else if(this.appName == 'treasury') {
      this.appTitle = 'Treasury Manager';
    } else {
      this.appTitle = 'Unknown';
    }
  }

  setCalendarGamingDay(index: any, gday: any) {
    console.log('Setting day ', index, gday);
  }

  getFontSize(){

  }

}
