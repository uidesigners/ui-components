import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ui-components';

  isChipsCard:boolean = false;
  cardTitle:any;
  cages = [
    {'name': 'Cage C01'},
    {'name': 'Cage C02'},
    {'name': 'Cage C03'},
    {'name': 'Cage C04'}
  ]
  
}
