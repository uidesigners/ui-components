import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { WdDesignServiceService } from '../services/wd-design-service.service';
import {Router} from '@angular/router';
import { DOCUMENT } from '@angular/common';



@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.scss']
})
export class MainContainerComponent implements OnInit {

  @HostListener ('document:fullscreenchange', ['$event'])
  @HostListener ('document:webkitfullscreenchange', ['$event'])
  @HostListener ('document:mozfullscreenchange', ['$event'])
  @HostListener ('document:MSFullscreenChange', ['$event'])

  elem: any;
  isFullScreen: boolean = false;
  doc: any;

  constructor( 
    private _designService: WdDesignServiceService,
    public router: Router,
    @Inject(DOCUMENT) private document: any) { }

  ngOnInit(): void {
    // this._designService.btnBack.next(this.backButton);
    this.elem = document.documentElement;
  }

  onResize(evt: any){
    console.log(evt);
  }

  // ngOnDestroy(): void {
    // this._designService.btnBack.next(!this.backButton);
  // }

  goFullscreen(){
    if(this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } else if(this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
    }
    this.isFullScreen = true;
  }

  // This snippet 'exitFullscreen()' does not work;
  // So, putting a msg for the user to use 'esc' key to exit 'fullscreen' mode;

  exitFullscreen() {
    if(this.document.exitFullscreen) {
      this.document.exitFullscreen().catch(() => { });
    } else if(this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
    this.isFullScreen = false;
  }
  
}
