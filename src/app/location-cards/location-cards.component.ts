import { Component, OnDestroy, OnInit } from '@angular/core';
import { WdDesignServiceService } from '../services/wd-design-service.service';

@Component({
  selector: 'app-location-cards',
  templateUrl: './location-cards.component.html',
  styleUrls: ['./location-cards.component.scss']
})
export class LocationCardsComponent implements OnInit {
  
  isChipsCard:boolean = false;
  cardTitle:any;
  cages:any;

  constructor(private _designService: WdDesignServiceService) { }

  ngOnInit(): void {
    this._designService.cages.subscribe(res => {
      this.cages = res;
    })
  }

  // ngOnDestroy(): void {
  //     this._designService.cages.unsubscribe();
  // }

}
