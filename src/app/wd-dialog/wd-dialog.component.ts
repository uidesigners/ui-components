import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../subnav-data-bar/subnav-data-bar.component';

@Component({
  selector: 'app-wd-dialog',
  templateUrl: './wd-dialog.component.html',
  styleUrls: ['./wd-dialog.component.scss']
})
export class DialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

  table:any = {
    name: this.data.name
  }

}
