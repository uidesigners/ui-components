import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dialog-add-player',
  templateUrl: './dialog-add-player.component.html',
  styleUrls: ['./dialog-add-player.component.scss']
})
export class DialogAddPlayerComponent implements OnInit {

  addPlayer: boolean = true;
  addAgent: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  
  onSlideToggleChecked(evt:any):void{
    if(evt.checked){
      this.addAgent = true;
      this.addPlayer = false;
    } else {
      this.addAgent = false;
      this.addPlayer = true;
    }
  }
  
  onSlideToggle(evt:any) {
    if(evt.checked){
      this.addAgent = false;
      this.addPlayer = true;
    } else {
      this.addAgent = true;
      this.addPlayer = false;
    }
  }



}
