import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogAddPlayerComponent } from './dialog-add-player/dialog-add-player.component';


@Component({
  selector: 'app-tab-buyins',
  templateUrl: './tab-buyins.component.html',
  styleUrls: ['./tab-buyins.component.scss']
})
export class TabBuyinsComponent implements OnInit {

  invalidPanels = [
    {'csname': 1, 'status': 'invalid'}
  ];

  panels = [
    {'csname': 1, 'status': 'valid'},
    {'csname': 3, 'status': 'not-allowed'},
    {'csname': 4, 'status': 'not-allowed'},
    {'csname': 5, 'status': 'not-allowed'},
    {'csname': 6, 'status': 'valid'},
    {'csname': 7, 'status': 'valid'},
    {'csname': 9, 'status': 'valid'},
    {'csname': 10, 'status': 'valid'},
    {'csname': 12, 'status': 'valid'}
  ];

  isChipsPlaced: boolean = false;
  isChipsInvalid: boolean = false;
  isAggregationBtnEnabled: boolean = false;
  isTransactionConfirmed: boolean = false;
  isMsgRemoveChips: boolean = false;
  areRestLinksDisable: boolean = false;

  buyInTypes: string[] = ['Cash', 'Credit Card', 'Front Money', 'Promotion', 'Marker', 'Ticket', 'Disable Links'];
  buyInTypesDefault: string = 'Cash';

  constructor(public wdDialog: MatDialog) { }

  ngOnInit(): void {
  }

  onPlacingChips(evt: any){
    this.showChipsetsPlaced();
  }

  showChipsetsPlaced(){
    this.isChipsPlaced = true;
    this.isChipsInvalid = true;
    this.makeAggregationBtnEnabled();
  }

  makeAggregationBtnEnabled() {
    if(this.isChipsInvalid || !this.isChipsPlaced) {
      this.isAggregationBtnEnabled = true;
      console.log('btn enabled');
    } else {
      this.isAggregationBtnEnabled = false;
      console.log('btn disabled');
    }

    return this.isAggregationBtnEnabled;
  }

  onRemovingInvalidChips(){
    this.showValidChipsets();
  }

  showValidChipsets(){
    this.isChipsInvalid = false;
  }

  onAggregatingChips(){

  }

  onBtnCancel(){
    this.reset();
  }

  onBtnConfirm(){
    this.showChipsRemoveMsg();
    this.onDisableFooterLinks();
  }

  showChipsRemoveMsg() {
    return this.isMsgRemoveChips = true;
  }

  onDisableFooterLinks(){
    let footerLinks = document.querySelectorAll(".sub-nav__tab-link:not(.mat-tab-label-active)");

    for(let link in footerLinks){
      console.log(link[0]);
    }
  }

  onSelectType(evt:any){
    if(evt.value == 7){
      this.areRestLinksDisable = true;
    } else {
      this.areRestLinksDisable = false;
    }
  }

  onBtnAddAgent(evt:any){
    // console.log(evt.target.innerHTML);
    this.openAddAgentDialog(evt);
  }

  openAddAgentDialog(data:any){
    const dialogRef = this.wdDialog.open(DialogAddPlayerComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('hello from dialog');
    });
  }

  reset(){
    this.isChipsPlaced = false;
    this.isChipsInvalid = false;
    this.isAggregationBtnEnabled = false;
    this.isTransactionConfirmed = false;
    this.isMsgRemoveChips = false;
  }

}
