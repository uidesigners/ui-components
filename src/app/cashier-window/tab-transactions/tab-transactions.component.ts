import { Component, OnInit } from '@angular/core';

interface Table {
  Time: any,
  ID: Number,
  Transaction: Number,
  Cashier: String,
  Approver: String,
  Value: Number,
  Player: String,
  Agent: String,
  Notes: String
}

@Component({
  selector: 'app-tab-transactions',
  templateUrl: './tab-transactions.component.html',
  styleUrls: ['./tab-transactions.component.scss']
})
export class TabTransactionsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  columnTitles: any = ['Time', 'ID', 'Transaction', 'Cashier', 'Approver', 'Value', 'Player', 'Agent', 'Notes'];
  tableData:any = [
    {
      'Time': '14:30', 
      'ID': 1199,
      'Transaction': 'Buy In (Cash)',
      'Cashier': 123008,
      'Approver': '',
      'Value': 1600000,
      'Player': 'Anonymous (90001)',
      'Agent': 'Gupta, Amit (110000)',
      'Notes': ''
    },
    {
      'Time': '14:30', 
      'ID': 1199,
      'Transaction': 'Buy In (Cash)',
      'Cashier': 123008,
      'Approver': '',
      'Value': 1600000,
      'Player': 'Anonymous (90001)',
      'Agent': 'Gupta, Amit (110000)',
      'Notes': ''
    },
    {
      'Time': '14:30', 
      'ID': 1199,
      'Transaction': 'Buy In (Cash)',
      'Cashier': 123008,
      'Approver': '',
      'Value': 1600000,
      'Player': 'Anonymous (90001)',
      'Agent': 'Gupta, Amit (110000)',
      'Notes': ''
    },
    {
      'Time': '14:30', 
      'ID': 1199,
      'Transaction': 'Buy In (Cash)',
      'Cashier': 123008,
      'Approver': '',
      'Value': 1600000,
      'Player': 'Anonymous (90001)',
      'Agent': 'Gupta, Amit (110000)',
      'Notes': ''
    },

  ]


}
