import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab-chipdetail',
  templateUrl: './tab-chipdetail.component.html',
  styleUrls: ['./tab-chipdetail.component.scss']
})
export class TabChipdetailComponent implements OnInit {

  invalidPanels = [
    {'csname': 1, 'status': 'invalid'}
  ];

  panels = [
    {'csname': 1, 'status': 'valid'},
    {'csname': 3, 'status': 'not-allowed'},
    {'csname': 4, 'status': 'not-allowed'},
    {'csname': 5, 'status': 'not-allowed'},
    {'csname': 6, 'status': 'valid'},
    {'csname': 7, 'status': 'valid'},
    {'csname': 9, 'status': 'valid'},
    {'csname': 10, 'status': 'valid'},
    {'csname': 12, 'status': 'valid'}
  ];

  isChipsPlaced: boolean = false;
  isChipsInvalid: boolean = false;
  isAggregationBtnEnabled: boolean = false;
  isTransactionConfirmed: boolean = false;
  isMsgRemoveChips: boolean = false;
  areRestLinksDisable: boolean = false;

  localEle = document.querySelector('.cashier-window__buy-in-types');

  buyInTypes: string[] = ['Cash', 'Credit Card', 'Front Money', 'Promotion', 'Marker', 'Ticket', 'Disable Links'];
  buyInTypesDefault: string = 'Cash';

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    console.log(this.localEle);
  }
  
  onPlacingChips(evt: any){
    this.showChipsetsPlaced();
  }

  showChipsetsPlaced(){
    this.isChipsPlaced = true;
    this.isChipsInvalid = true;
    this.makeAggregationBtnEnabled();
  }

  makeAggregationBtnEnabled() {
    if(this.isChipsInvalid || !this.isChipsPlaced) {
      this.isAggregationBtnEnabled = true;
      console.log('btn enabled');
    } else {
      this.isAggregationBtnEnabled = false;
      console.log('btn disabled');
    }

    return this.isAggregationBtnEnabled;
  }

  onRemovingInvalidChips(){
    this.showValidChipsets();
  }

  showValidChipsets(){
    this.isChipsInvalid = false;
  }

  onAggregatingChips(){

  }

  onBtnCancel(){
    this.reset();
  }

  onBtnConfirm(){
    this.showChipsRemoveMsg();
    this.onDisableFooterLinks();
  }

  showChipsRemoveMsg() {
    return this.isMsgRemoveChips = true;
  }

  onDisableFooterLinks(){
    let footerLinks = document.querySelectorAll(".sub-nav__tab-link:not(.mat-tab-label-active)");

    for(let link in footerLinks){
      console.log(link[0]);
    }
  }

  onSelectType(evt:any){
    if(evt.value == 7){
      this.areRestLinksDisable = true;
    } else {
      this.areRestLinksDisable = false;
    }
  }

  reset(){
    this.isChipsPlaced = false;
    this.isChipsInvalid = false;
    this.isAggregationBtnEnabled = false;
    this.isTransactionConfirmed = false;
    this.isMsgRemoveChips = false;
  }

}
