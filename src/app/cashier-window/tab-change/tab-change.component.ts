import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-tab-change',
  templateUrl: './tab-change.component.html',
  styleUrls: ['./tab-change.component.scss']
})
export class TabChangeComponent implements OnInit {

  panels = [1,2,3,4,5,6,7,8,9,10];

  isCardChipsInActive = true;
  isCardChipsOutActive = false;
  isInitiatingChange = true;
  isChipsInPlaced = false;
  isChipsOutPlaced = false;
  isMoreChipsInPlaced = false;
  isMoreChipsOutPlaced = false;
  isAggregatingChipsIn = false;
  isAggregatingChipsOut = false;
  isAddingMoreChipsets = false;
  isChipsetsCardBtnActive = false;
  isChipsetsCardActive = false;
  isMsgPlaceChipsOut = false;
  isChangeMismatched = false;
  isChangeMatched = false;
  isMsgRemoveChips = false;
  timeCounter:number = 2;
  
  constructor(private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    const counter = setInterval(() => {
      if(this.timeCounter <= 0){
        clearInterval(counter);
        this.onPlacingChipsIn();
      } else {
        this.timeCounter --;
      }
    }, 1000);
  }

  onPlacingChipsIn(){
    this.isInitiatingChange = false;
    this.isChipsInPlaced = true;
  }

  onAggregatingChipsIn(){
    this.isMoreChipsInPlaced = true;
    this.isCardChipsOutActive = true;
  }

  onMsgPlaceChipsOut(){
    this.isCardChipsInActive = false;
    this.isMsgPlaceChipsOut = true;
    this.isCardChipsOutActive = true;
  }

  onPlacingChipsOut(){
    this.isMsgPlaceChipsOut = false;
    this.isChipsOutPlaced = true;
    this.checkChipsetMatch(1);
  }

  onAggregatingChipsOut(){
    this.isMoreChipsOutPlaced = true;
  }

  checkChipsetMatch(val: number){
    if(val == 1) {
      this.isChangeMismatched = true;
      this.isChangeMatched = false;
    } else if(val == 2){
      this.isChangeMismatched = false;
      this.isChangeMatched = true;
    }
  }

  onAggregatingChips(){
    if(this.isCardChipsInActive) {
      this.isChipsInPlaced = false;
      this.isAggregatingChipsIn = true;
    } else {
      this.checkChipsetMatch(1);
      this.onAggregatingChipsOut();
      this.isChipsOutPlaced = false;
      this.isAggregatingChipsOut = true;
    }
  }

  onRadioChange(evt: any){
    console.log(evt.value);
    if(evt.value == 1){
      this.checkChipsetMatch(1);
    } else if(evt.value == 2) {
      this.checkChipsetMatch(2);
    } else {
      this.onBtnConfirm();
    }
  }

  onBtnConfirm(){
    console.log('confirm');
    this.isMsgRemoveChips = true;
    this.showSnackBar();
  }
  
  showSnackBar(){
    this._snackBar.open('Change successful', '',{
      duration: 30000,
      horizontalPosition: 'right',
      panelClass: 'snack__success'
    });
  }

}
