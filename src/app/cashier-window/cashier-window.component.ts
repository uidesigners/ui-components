import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cashier-window',
  templateUrl: './cashier-window.component.html',
  styleUrls: ['./cashier-window.component.scss']
})
export class CashierWindowComponent implements OnInit {

  cwName: string= 'Cashier-Window-01';
  cwGamingDay: string= '17-Jan';
  cwGamingTime: string= '22:38';
  cwUserNameID: string= 'Gupta, Amit (123001)';

  isChipsInvalid: boolean = false;
  areRestLinksDisable: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  onRemovingInvalidChips(){}

}
