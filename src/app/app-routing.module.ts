import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './home/dashboard.component';
import { LocationCardsComponent } from './location-cards/location-cards.component';
import { MainContainerComponent } from './main-container/main-container.component';
import { SubnavDataBarComponent } from './subnav-data-bar/subnav-data-bar.component';
import { CashierWindowComponent } from './cashier-window/cashier-window.component';
import { TabChangeComponent } from './cashier-window/tab-change/tab-change.component';
import { AppTopNavBarComponent } from './top-nav-bar/app-top-nav-bar.component';
import { BreadcrumbsComponent } from './ui-components/breadcrumbs/breadcrumbs.component';
import { ButtonComponent } from './ui-components/button/button.component';
import { ChipsetComponent } from './ui-components/chipset/chipset.component';
import { FiltersComponent } from './ui-components/filters/filters.component';
import { IconsComponent } from './ui-components/icons/icons.component';
import { SnackbarsComponent } from './ui-components/snackbars/snackbars.component';
import { TabsComponent } from './ui-components/tabs/tabs.component';
import { BottomNavComponent } from './ui-components/bottom-nav/bottom-nav.component';
import { WdSpinnerComponent } from './ui-components/wd-spinner/wd-spinner.component';
import { TabBuyinsComponent } from './cashier-window/tab-buyins/tab-buyins.component';
import { TabTransactionsComponent } from './cashier-window/tab-transactions/tab-transactions.component';
import { TabChipdetailComponent } from './cashier-window/tab-chipdetail/tab-chipdetail.component';
import { TabCashoutComponent } from './cashier-window/tab-cashout/tab-cashout.component';
import { TabUpdateownershipComponent } from './cashier-window/tab-updateownership/tab-updateownership.component';
import { WdTableComponent } from './wd-table/wd-table.component';
import { WdBlocksComponent } from './ui-components/wd-blocks/wd-blocks.component';
import { ChipsetsComponent } from './ui-components/chipsets/chipsets.component';
import { WdMenuComponent } from './ui-components/wd-menu/wd-menu.component';
import { AnimationsComponent } from './animations/animations.component';
import { TreasuryComponent } from './treasury/treasury.component';
import { TotalChipsComponent } from './treasury/total-chips/total-chips.component';
import { WdTooltipComponent } from './ui-components/wd-tooltip/wd-tooltip.component';
import { WdTablesComponent } from './ui-components/wd-tables/wd-tables.component';
import { WdSnackbarComponent } from './ui-components/snackbars/wd-snackbar/wd-snackbar.component';
import { WdDealerDisplayComponent } from './wd-dealer-display/wd-dealer-display.component';
import { ClusterAntennaComponent } from './cluster-antenna/cluster-antenna.component';
import { GameTemplateDialogComponent } from './game-template-dialog/game-template-dialog.component';
import { BuyInsComponent } from './buy-ins/buy-ins.component';

const appRoutes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'top-nav-bar', component: AppTopNavBarComponent },
  { path: 'sub-nav-bar', component: SubnavDataBarComponent },
  { path: 'bottom-nav', component: BottomNavComponent },
  { path: 'location-cards', component: LocationCardsComponent },
  { path: 'change-tab', component: TabChangeComponent },
  { path: 'chipset', component: ChipsetComponent },
  { path: 'tabs', component: TabsComponent },
  { path: 'icons', component: IconsComponent },
  { path: 'spinner', component: WdSpinnerComponent },
  { path: 'wd-snackbar', component: WdSnackbarComponent },
  { path: 'snackbars', component: SnackbarsComponent },
  { path: 'buttons', component: ButtonComponent },
  { path: 'filters', component: FiltersComponent },
  { path: 'breadcrumbs', component: BreadcrumbsComponent },
  { path: 'tables', component: WdTableComponent },
  { path: 'components', component: MainContainerComponent },
  { path: 'blocks', component: WdBlocksComponent },
  { path: 'chipsets', component: ChipsetsComponent },
  { path: 'menu', component: WdMenuComponent },
  { path: 'tooltip', component: WdTooltipComponent},
  { path: 'animations', component: AnimationsComponent},
  { path: 'ag-game-template', component: GameTemplateDialogComponent},
  {
    path: 'cashier-window', component: CashierWindowComponent, children: [
      { path: '', component: TabBuyinsComponent },
      { path: 'buyins', component: TabBuyinsComponent },
      { path: 'cashout', component: TabCashoutComponent },
      { path: 'change', component: TabChangeComponent },
      { path: 'updateownership', component: TabUpdateownershipComponent },
      { path: 'chipdetail', component: TabChipdetailComponent },
      { path: 'transactions', component: TabTransactionsComponent },
    ]
  },
  { path: 'treasury', component: TreasuryComponent },
  { path: 'totalchips', component: TotalChipsComponent},
  { path: 'tabletypes', component: WdTablesComponent},
  { path: 'dealer-display', component: WdDealerDisplayComponent},
  { path: 'cluster-antenna', component: ClusterAntennaComponent},
  { path: 'buy-ins', component: BuyInsComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
