import { Component, OnInit } from '@angular/core';
import { WdDesignServiceService } from '../services/wd-design-service.service';
import {Router} from '@angular/router'

@Component({
  selector: 'app-treasury',
  templateUrl: './treasury.component.html',
  styleUrls: ['./treasury.component.scss', './summary-block.scss', './player-enlarged.scss']
})
export class TreasuryComponent implements OnInit {
  isChipsCard:boolean = false;
  cardTitle:any;
  cagesLvl0:any;
  totalChipsPath: string = "totalchips";
  playerImage = '/assets/images/player.png';
  zoomPlayer:boolean = false;
  summaryBlockData = [
    { "key": "Player", "value": "Herrara, Chris" },
    { "key": "ID", "value": 123005 },
    { "key": "Status", "value": "Open" },
    { "key": "Player W/L", "value": 1000 },
    { "key": "Theo Win", "value": 0 },
    { "key": "Avg Bet", "value": 1000 },
    { "key": "Buy In", "value": 0 },
    { "key": "Bankroll", "value": 2700 },
    { "key": "Time Played", "value": "00:00:46" },
    { "key": "Last Location", "value": "Table-205" },
    { "key": "Last Active", "value": "10-Feb-2022 10:45:42" }
  ];

  constructor( private _designService: WdDesignServiceService,
    public router: Router) { }

  ngOnInit(): void {
    this._designService.cagesLvl0.subscribe(res => {
      this.cagesLvl0 = res;
    })

    console.log(this.summaryBlockData)
  }

  onClick(evt:any){
    this.router.navigate(['totalchips']);
  }

  onPlayerZoom(){
    this.zoomPlayer = !this.zoomPlayer;
  }
  

}
