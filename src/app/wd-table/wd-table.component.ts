import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

export interface Roles {
  status: string;
  name: string;
  site: string;
  users: any;
  createdon: string;
  createdby: string;
  modifiedon: any;
  modifiedby: any;
  expandicon: any
}

const WD_TABLE_DATA: Roles[] = [
  {
    status: 'active',
    name: 'BI - Access',
    site: 'site-1',
    users: 2,
    createdon: '2022-01-06T07:40:35Z',
    createdby: 'Master, PP (0)',
    modifiedon: null,
    modifiedby: null,
    expandicon: null
  },
  {
    status: 'active',
    name: 'Configuration',
    site: 'site-2',
    users: 2,
    createdon: '2022-01-06T07:40:35Z',
    createdby: 'Master, PP (0)',
    modifiedon: null,
    modifiedby: null,
    expandicon: null
  },
  {
    status: 'active',
    name: 'Casino Manager',
    site: 'site-3',
    users: 2,
    createdon: '2022-01-06T07:40:35Z',
    createdby: 'Master, PP (0)',
    modifiedon: null,
    modifiedby: null,
    expandicon: null
  },
  {
    status: 'active',
    name: 'Table Dashboard',
    site: 'site-4',
    users: 2,
    createdon: '2022-01-06T07:40:35Z',
    createdby: 'Master, PP (0)',
    modifiedon: null,
    modifiedby: null,
    expandicon: null
  },
];


@Component({
  selector: 'app-wd-table',
  templateUrl: './wd-table.component.html',
  styleUrls: ['./wd-table.component.scss']
})
export class WdTableComponent implements OnInit {

  constructor( private _liveAnnouncer: LiveAnnouncer) { }

  ngOnInit(): void {
  }

  displayedColumns: string[] = ['status', 'name', 'site', 'users', 'createdon', 'createdby', 'modifiedon', 'modifiedby','expandicon'];
  wdDataArray = new MatTableDataSource(WD_TABLE_DATA);
  isCellEditable:boolean = false;
  cellIdx:any;

  clickedRows = new Set<Roles>();

  onEdit(evt:any, elem:any){
    this.cellIdx = elem;
    this.isCellEditable = true;
  }

  onCellBlur() {
    this.isCellEditable = false;
    // alert('Blurred');
  }

  onSubmit(){
    alert('submitted');
  }

  onDismiss(){
    this.isCellEditable = false;
  }

  @ViewChild(MatSort)
  sort!: MatSort;

  ngAfterViewInit() {
    this.wdDataArray.sort = this.sort;
  }

  announceSortChange(sortState: Sort) {
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
  

}
