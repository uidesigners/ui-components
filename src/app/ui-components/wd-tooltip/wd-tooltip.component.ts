import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-wd-tooltip',
  templateUrl: './wd-tooltip.component.html',
  styleUrls: ['./wd-tooltip.component.scss']
})
export class WdTooltipComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  tooltipCustomMessage = new FormControl('It is a custom long tooltip that can be used to show description details against some feature of the application and let the user know how to use this or that.');

}
