import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  playerImage: any = "/assets/images/player.png";

  constructor() { }

  ngOnInit(): void {
  }

  clicked = false;

  appTypes:Array<string> = ["casino-manager", "configuration", "player-dashboard", "treasury", "alerts", "reports", "cam", "cashier", "table-dash" ]
}

