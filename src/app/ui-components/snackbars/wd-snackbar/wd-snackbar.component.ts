import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-wd-snackbar',
  templateUrl: './wd-snackbar.component.html',
  styleUrls: ['./wd-snackbar.component.scss']
})
export class WdSnackbarComponent implements OnInit {

  constructor( private _snackBar: MatSnackBar ) { }

  ngOnInit(): void {
  }

  message:string = "Message:";
  action:string = "Hello!"
  openSnackBar(message: string, action: string) {
    if(message.toLocaleLowerCase() == "success") {
      this._snackBar.open(message, action, {
        panelClass: 'snack__success'
      });
    } else if(message.toLocaleLowerCase() == "warn") {
      this._snackBar.open(message, action, {
        panelClass: 'snack__warn'
      });
    } else if(message.toLocaleLowerCase() == "error") {
      this._snackBar.open(message, action, {
        panelClass: 'snack__error'
      });
    } else if(message.toLocaleLowerCase() == "info") {
      this._snackBar.open(message, action, {
        panelClass: 'snack__info'
      });
    }
  }
  

}
