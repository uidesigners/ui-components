import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wd-blocks',
  templateUrl: './wd-blocks.component.html',
  styleUrls: ['./wd-blocks.component.scss']
})
export class WdBlocksComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  fillCount: number = 90;
  creditCount: number = 90;
  fillValue: number = 990000;
  creditValue: number = 990000;
  buyinCount: number = 16;
  buyinValue: number = 412700;

}
