import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.scss']
})
export class IconsComponent implements OnInit {

  isIconsPrimary:boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  onChboxChange(evt: any){
    if(evt.checked){
      this.isIconsPrimary = true;
    } else {
      this.isIconsPrimary = false;
    }
  }

}
