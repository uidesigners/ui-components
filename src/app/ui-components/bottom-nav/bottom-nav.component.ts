import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bottom-nav',
  templateUrl: './bottom-nav.component.html',
  styleUrls: ['./bottom-nav.component.scss']
})
export class BottomNavComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  tabs = ["Buy In", "Cash Out", "Change", "Update Ownership", "Chip Detail", "Transactions"];
  activeTab = this.tabs[0];
}
