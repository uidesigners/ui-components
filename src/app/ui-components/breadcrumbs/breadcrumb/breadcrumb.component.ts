import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  @Input('appName') locAppName: string = '';

  constructor() { }

  ngOnInit(): void {
    
  }

  currentLevel: string = "";

  breadCrumbArray: Array<string> = ['Site', 'Gaming Area', 'Operating Area', 'Pit', 'Table'];

  clickEvnt($item: any, $index: number): void {
    // console.log($item +' having index as ' + $index);
    this.currentLevel = $item;
  }

}
