import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {

  hide = true;
  
  constructor() { }

  ngOnInit(): void {
  }

  panels = [
    {'csname': 1, 'status': 'valid'},
    {'csname': 2, 'status': 'invalid'},
    {'csname': 3, 'status': 'not-allowed'},
    {'csname': 4, 'status': 'not-allowed'},
    {'csname': 5, 'status': 'not-allowed'},
    {'csname': 6, 'status': 'valid'},
    {'csname': 7, 'status': 'valid'},
    {'csname': 8, 'status': 'invalid'},
    {'csname': 9, 'status': 'valid'},
    {'csname': 10, 'status': 'valid'},
    {'csname': 11, 'status': 'invalid'},
    {'csname': 12, 'status': 'valid'}
  ];

  viewTypes = [
    {'name': 'EXPAND_ALL', 'value': 'open'},
    {'name': 'COLLAPSE_ALL', 'value': 'close'}
  ];

  toggleState = "open";

  changeView(view:any) {
    this.toggleState = view.value;
  }

}
