import { AfterViewInit, Component, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Component({
  selector: 'app-chipsets',
  templateUrl: './chipsets.component.html',
  styleUrls: ['./chipsets.component.scss']
})
export class ChipsetsComponent implements OnInit {

  hideFooter:boolean = false;

  constructor() { }

  ngOnInit(): void {

  }

  panels = [
    {'csname': 51, 'status': 'default'},
    {'csname': 42, 'status': 'valid'},
    {'csname': 23, 'status': 'not-allowed'},
    {'csname': 99, 'status': 'invalid'}
  ];

  onChange(ele:any){
    this.updateChipSetDesign(ele);
  }

  ngOnChanges(ele:any){
    return this.hideFooter;
  }

  updateChipSetDesign(ele:any){
    if(ele.checked){
      this.hideFooter= true;
    } else {
      this.hideFooter = false;
    }
  }
   
}
