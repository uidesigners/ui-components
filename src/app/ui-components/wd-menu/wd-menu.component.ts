import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-wd-menu',
  templateUrl: './wd-menu.component.html',
  styleUrls: ['./wd-menu.component.scss']
})
export class WdMenuComponent implements OnInit {

  @Input() showGamingDay: boolean = true;
  @Input() nextRollTIme: any;
  @Input() gamingDay: string = '';
  @Input() gamingDaysArr: Array<any> = ['10-Feb', '11-Feb'];

  loginPagesFlag: boolean = false;
  calendarDate: any;
  isAppOpened: boolean = true;
  userDetails: any = ['Chinese', 'Logout'];
  currentDate: string = "10-Feb"
  
  constructor() { }

  ngOnInit(): void {
  }

  setCalendarGamingDay(index: any, gday: any) {
    console.log('Setting day ', index, gday);
  }

}
