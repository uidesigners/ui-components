import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-chipset',
  templateUrl: './chipset.component.html',
  styleUrls: ['./chipset.component.scss']
})
export class ChipsetComponent implements OnInit {

  @Input('csName') 'locCSName': number;
  @Input('csStatus') 'locCSStatus': string;
  @Input() hideFooter: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }



}
