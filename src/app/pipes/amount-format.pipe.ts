import { Pipe, PipeTransform } from '@angular/core';

/*
 Pipe that converts value to either whole number or to 2 decimal places
 */


@Pipe({
  name: 'amountFormat'
})

export class AmountFormatPipe implements PipeTransform {
  transform(value: any): string {
    if (!value) {
      return value;
    }
    value = Number(value);
    if (Math.round(value) === value) {
      value = Math.round(value);
    } else if (value) {
      value = value.toFixed(2).replace(/\.0+$/, '');
    }
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

  }
}