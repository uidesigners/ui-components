import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAccordion, MatExpansionModule} from '@angular/material/expansion';

@Component({
  selector: 'app-dialog-game-template',
  templateUrl: './dialog-game-template.component.html',
  styleUrls: ['./dialog-game-template.component.scss']
})
export class DialogGameTemplateComponent implements OnInit {

  templateType = 'game'
  disableSelect = new FormControl(false);
  panel1OpenState = false;
  panel2OpenState = false;
  hideSection = false;
  isClicked1 = false;
  isClicked2 = false;
  isClicked3 = false;
  isClicked4 = false;

  UnassignedBets: string[] = ['Tiger Tie (TT)', 'Tiger Pair (TP)'];
  AssignedBets: string[] = ['Tie (TT)', 'Small Tiger (ST)', 'Big Tiger (BT)'];
  
  constructor() { }

  ngOnInit(): void {
  }
  
  toggleLeftPanel(){
    console.log('closed');
    if(!this.hideSection) {
      this.hideSection = true;
    } else {
      this.hideSection = false;
    }
    
  }

  

}
