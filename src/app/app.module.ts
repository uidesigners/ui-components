import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { MatDividerModule } from '@angular/material/divider';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule} from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CdkTableModule } from '@angular/cdk/table';
import {  MatButtonToggleModule } from '@angular/material/button-toggle';
import {MatListModule} from '@angular/material/list';

import { AppComponent } from './app.component';
import { AppTopNavBarComponent } from './top-nav-bar/app-top-nav-bar.component';
import { MainContainerComponent } from './main-container/main-container.component';
import { SubnavDataBarComponent } from './subnav-data-bar/subnav-data-bar.component';
import { ChipsetComponent } from './ui-components/chipset/chipset.component';
import { FilterComponent } from './filter/filter.component';
import { DialogComponent } from './wd-dialog/wd-dialog.component';
import { LocationCardsComponent } from './location-cards/location-cards.component';
import { LocationCardComponent } from './location-cards/location-card/location-card.component';
import { WdTableComponent } from './wd-table/wd-table.component';
import { DashboardComponent } from './home/dashboard.component';
import { ButtonComponent } from './ui-components/button/button.component';
import { FiltersComponent } from './ui-components/filters/filters.component';
import { TabsComponent } from './ui-components/tabs/tabs.component';
import { IconsComponent } from './ui-components/icons/icons.component';
import { SnackbarsComponent } from './ui-components/snackbars/snackbars.component';
import { BreadcrumbsComponent } from './ui-components/breadcrumbs/breadcrumbs.component';
import { WdSpinnerComponent } from './ui-components/wd-spinner/wd-spinner.component';
import { WdSnackbarComponent } from './ui-components/snackbars/wd-snackbar/wd-snackbar.component';
import { TabChangeComponent } from './cashier-window/tab-change/tab-change.component';
import { BreadcrumbComponent } from './ui-components/breadcrumbs/breadcrumb/breadcrumb.component';
import { BottomNavComponent } from './ui-components/bottom-nav/bottom-nav.component';
import { CashierWindowComponent } from './cashier-window/cashier-window.component';
import { WdIconComponent } from './ui-components/icons/wd-icon/wd-icon.component';
import { TabBuyinsComponent } from './cashier-window/tab-buyins/tab-buyins.component';
import { TabCashoutComponent } from './cashier-window/tab-cashout/tab-cashout.component';
import { TabUpdateownershipComponent } from './cashier-window/tab-updateownership/tab-updateownership.component';
import { TabChipdetailComponent } from './cashier-window/tab-chipdetail/tab-chipdetail.component';
import { TabTransactionsComponent } from './cashier-window/tab-transactions/tab-transactions.component';
import { DialogAddPlayerComponent } from './cashier-window/tab-buyins/dialog-add-player/dialog-add-player.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { WdBlocksComponent } from './ui-components/wd-blocks/wd-blocks.component';
import { AmountFormatPipe } from './pipes/amount-format.pipe';
import { ChipsetsComponent } from './ui-components/chipsets/chipsets.component';
import { WdMenuComponent } from './ui-components/wd-menu/wd-menu.component';
import { AnimationsComponent } from './animations/animations.component';
import { TreasuryComponent } from './treasury/treasury.component';
import { TotalChipsComponent } from './treasury/total-chips/total-chips.component';
import { WdTooltipComponent } from './ui-components/wd-tooltip/wd-tooltip.component';
import { WdTablesComponent } from './ui-components/wd-tables/wd-tables.component';
import { WdDealerDisplayComponent } from './wd-dealer-display/wd-dealer-display.component';
import { ClusterAntennaComponent } from './cluster-antenna/cluster-antenna.component';
import { GameTemplateDialogComponent } from './game-template-dialog/game-template-dialog.component';
import { DialogGameTemplateComponent } from './game-template-dialog/dialog-game-template/dialog-game-template/dialog-game-template.component';
import { BuyInsComponent } from './buy-ins/buy-ins.component';

@NgModule({
  declarations: [
    AppComponent,
    AppTopNavBarComponent,
    MainContainerComponent,
    SubnavDataBarComponent,
    ChipsetComponent,
    FilterComponent,
    DialogComponent,
    LocationCardsComponent,
    LocationCardComponent,
    WdTableComponent,
    DashboardComponent,
    ButtonComponent,
    FiltersComponent,
    TabsComponent,
    IconsComponent,
    SnackbarsComponent,
    BreadcrumbsComponent,
    WdSpinnerComponent,
    WdSnackbarComponent,
    TabChangeComponent,
    BreadcrumbComponent,
    BottomNavComponent,
    CashierWindowComponent,
    WdIconComponent,
    TabBuyinsComponent,
    TabCashoutComponent,
    TabUpdateownershipComponent,
    TabChipdetailComponent,
    TabTransactionsComponent,
    DialogAddPlayerComponent,
    WdBlocksComponent,
    AmountFormatPipe,
    ChipsetsComponent,
    WdMenuComponent,
    AnimationsComponent,
    TreasuryComponent,
    TotalChipsComponent,
    WdTooltipComponent,
    WdTablesComponent,
    WdDealerDisplayComponent,
    ClusterAntennaComponent,
    GameTemplateDialogComponent,
    DialogGameTemplateComponent,
    BuyInsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatDividerModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatExpansionModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatRippleModule,
    MatTabsModule,
    MatTooltipModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatSortModule,
    CdkTableModule,
    MatButtonToggleModule,
    MatListModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
