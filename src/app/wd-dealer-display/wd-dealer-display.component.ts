import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wd-dealer-display',
  templateUrl: './wd-dealer-display.component.html',
  styleUrls: ['./wd-dealer-display.component.scss', './wd-dd-container-transformed.scss']
})
export class WdDealerDisplayComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
